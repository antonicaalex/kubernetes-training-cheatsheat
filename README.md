# Kubernetes Training CheatSheat

This Repo contains basic/common commands that will help one to solve K8s CKAD faster.

> All commands will assume that you have run this alias first (it saves you precious time)

## Prerequisites before starting the exam
 To make your life easier while taking the exam, run this command

 ```
source <(kubectl completion bash) # setup autocomplete in bash into the current shell, bash-completion package should be installed first.
echo "source <(kubectl completion bash)" >> ~/.bashrc # add autocomplete permanently to your bash shell.
 ```

## Legend
- ns = namespace
- k = kubectl

### Alias
`alias k="kubectl"`

#### Create a namespace
`k create ns ckad-prep`

#### Create normal pod on a given ns
`k run mypod --image=nginx:1.15.12 --namespace=ckad-prep --port=80 --restart=Never`

#### Create pod from a yaml file
`k apply -f filename.pod`

#### Describe a pod from a ns
`k describe pod -n ckad-prer mypod`

#### Get IP of a pod on a given ns
`k get pod mypod -n ckad-prep -o wide`

#### Api information about a resource
`k explain pod.spec | grep -A 10 volumes`

#### Create temporary pod on a given namespace and sh into it
`k run busybox --image=busybox --rm -it --restart=Never -n ckad-prep -- /bin/sh`

#### Render the logs of a pod from a ns
`k logs -n ckad-prep mypod`

#### Render the logs of a container in a multi container pod from a ns
`k logs -n ckad-prep mypod -c mycontainer`

#### Edit a pod live from a ns
`k edit pod mypod -n ckad-prep`

#### Running a command on a pod
`k exec -it hello -- bash -c  "curl localhost:3000"`

#### Adding/Updating labels on a pod 
`kubectl label pods backend env=test`

#### Removing label from a pod
`kubectl label pods backend env-`

#### Create pod with labels
`kubectl run frontend --image=nginx --restart=Never --labels=env=prod,team=shiny`

#### Query podds by labels
`kubectl get pods -l 'team in (shiny, legacy)',env=prod --show-labels`

#### Delete pod from a ns
`k delete pod mypod -n ckad-prep`

#### Delete namespace
`k delete namespace ckad-prep`

#### Create configmap from file 
`k create configmap db-config --from-env-file=config.txt`

#### Connect to a pod from (from a ns)
`k exec -it mypod bash (or in some cases sh or /bin/sh) (-n mynamespace)`

### Make pod use config map
```
    k run backend --image=nginx --restart=Never --dry-run -o yaml > backend.yaml
    vim backend.yaml
    ....
    apiVersion: v1
    kind: Pod
    metadata:
    creationTimestamp: null
    labels:
        run: backend
    name: backend
    spec:
        containers:
        - image: nginx
          name: backend
          envFrom:
          - configMapRef:
              name: db-config
          resources: {}
        dnsPolicy: ClusterFirst
        restartPolicy: Never
    status: {}
```

#### Mount empty dir on a pod 
```
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: secured
  name: secured
spec:
  containers:
  - image: nginx
    name: secured
    resources: {}
    volumeMounts:
    - mountPath: /data/app
      name: app-volume
  volumes:
  - name: app-volume
    emptyDir: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Never
status: {}
```

#### Mount empty dir on a pod with security group at pod level
```
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: secured
  name: secured
spec:
  securityContext:
    fsGroup: 3000
  containers:
  - image: nginx
    name: secured
    volumeMounts:
    - name: data-vol
      mountPath: /data/app
    resources: {}
  volumes:
  - name: data-vol
    emptyDir: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Never
status: {}
```

#### Create resource quota at ns level
```
aantonica$ cat rq.yaml
apiVersion: v1
kind: ResourceQuota
metadata:
  name: app
spec:
  hard:
    pods: "2"
    requests.cpu: "2"
    requests.memory: 500m

----------------

k apply -f rq.yaml -n rq-demo
```

#### Create resource boundries at container level
```
aantonica$ cat rs.yaml 
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: rs
  name: rs
spec:
  containers:
  - image: nginx
    name: rs
    resources: 
      requests:
        memory: "1Gi"
        cpu: "1"
  dnsPolicy: ClusterFirst
  restartPolicy: Never
status: {}

----------

k apply -f rq.yaml -n rq-demo
```

#### Use service account in pod
```
aantonica$ cat rs.yaml 
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: rs
  name: rs
spec:
  serviceAccountName: myserviceaccount
  ....
```

#### Network poicies
This network policy will applied to all the pods that have the label `app` equal to `nginx` and it will allowd access only to those that have the
label `access` with value `true`
```
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: access-nginx
spec:
  podSelector:
    matchLabels:
      app: nginx
  ingress:
  - from:
    - podSelector:
        matchLabels:
          access: "true"
```

#### Create deployment
`$ kubectl create deployment deploy --image=nginx --dry-run -o yaml > deploy.yaml`

#### Change image on a deployment
`kubectl set image deployment/deploy nginx=nginx:latest`

#### Check oout rollout history
`kubectl rollout history deploy`

#### Scale a deployment
`kubectl scale deployments deploy --replicas=5`












